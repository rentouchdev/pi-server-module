# pi-server-module

Common functionality and data to use in pi micro-services


## pitracer
For auditing purpose all actions should be logged via pitracer. There are
multiple ways to log to pitracer using the pitracer module which is part of
this pi-server module.

### Usage examples
#### Init in your main (entrypoint)
```python
# Simple
from pi_server.pitracer import tracer
host = '127.0.0.1'  # host of pitracer-redis
port = '6379'  # port of pitracer-redis
tracer.init(host, port, 'authserver')

# or with logger configured
import logging
log = logging.getLogger('PIPlanningServer')
tracer.init(host, port, 'authserver', logger=log)
```

#### Usage throughout the code
```python
from pi_server.pitracer import tracer
company = 'quartabiz'
triggered_by = 'user'
action = 'create_sticky'
# minimal:
tracer.log(company, triggered_by, action)
# with data
data = {'foo': 'bar'}
tracer.log(company, triggered_by, action, data)
# with additional parameters (cols in table)
object_name = 'sticky'
object_id = '5ee79ef4f5b5237e8319c3c0'
tracer.log(company, triggered_by, action, data, object=object_name, 
           object_id=object_id)
```

### Deactivate tracer via env-variable
You can deactivate the tracer (won't do anything) if you set a env-variable. This
is useful if for some reason you need to turn it off in production.

`DEACTIVATE_PI_TRACER = True` default is False