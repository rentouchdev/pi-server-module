"""
To be able to run this script, do a:
- venv/bin/python setup.py develop (loosly links this into your env)
"""
import os
from twisted.internet.defer import inlineCallbacks
from twisted.internet import reactor
from pi_server.pitracer import Tracer
from autobahn.twisted.util import sleep

HOST = os.getenv('HOST')
PORT = int(os.getenv('PORT'))


@inlineCallbacks
def main():
    tracer = Tracer(host=HOST, port=PORT, service='authserver')
    yield sleep(.8)
    print('start push...')
    for i in range(20):
        data = {'index': i}
        yield tracer.log('quartabiz', 'test-script', 'range', data)
    reactor.stop()


if __name__ == '__main__':
    reactor.callWhenRunning(main)
    reactor.run()
