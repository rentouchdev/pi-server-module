import json
import datetime

from bson import ObjectId


class ObjJSONEncoder(json.JSONEncoder):
    """ Use this to convert BSON ObjectID's to strings"""
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime.datetime):
            return str(o)
        return json.JSONEncoder.default(self, o)


def decode_bson(bson_dict):
    """ Converts a python dict which contains BSON objects to a python dict
    which is JSON compatible.
    E.g. You it will convert all ObjectID's to strings.

    :param bson_dict: dictionary containing BSON objects
    :return: dict
    """
    return json.loads(ObjJSONEncoder().encode(bson_dict))
