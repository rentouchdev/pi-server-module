from pi_server.utils import decode_bson


def add_needed_filed(response):
    if isinstance(response, list):
        for obj in response:
            if isinstance(obj, dict) and obj.get('_id'):
                obj['id'] = obj['_id']
    else:
        if isinstance(response, dict) and response.get('_id'):
            response['id'] = response['_id']
    return response


def db_response(response):
    ret = decode_bson(response)
    return add_needed_filed(ret)
