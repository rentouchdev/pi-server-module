import logging
import txredisapi as redis
from twisted.internet.defer import inlineCallbacks, returnValue


class RedisQueue(object):
    """Simple Queue with Redis Backend"""
    def __init__(self, host, port, namespace='queue', on_connected=None):
        self.namespace = namespace
        self.rc = self.rcf = None

        @inlineCallbacks
        def con_established(result):
            self.rc = result
            print("Connection established to Redis")
            if on_connected:
                yield on_connected()

        def conn_failed(f):
            print("Was not able to connect to Redis")
            print(f.getTraceback())

        self.rcf = redis.ConnectionPool(host=host, port=port, poolsize=30)
        self.rcf.addCallback(con_established)
        self.rcf.addErrback(conn_failed)

    @inlineCallbacks
    def qsize(self, key):
        """Return the approximate size of the queue."""
        ret = yield self.rc.llen(key)
        returnValue(ret)

    @inlineCallbacks
    def empty(self):
        """Return True if the queue is empty, False otherwise."""
        ret = self.rc.qsize() == 0
        returnValue(ret)

    @inlineCallbacks
    def put(self, key, item):
        """Put item into the queue."""
        key = '%s:%s' % (self.namespace, key)
        if self.rc:
            yield self.rc.rpush(key, item)
