import logging

from autobahn.wamp.types import RegisterOptions
from twisted.internet.defer import inlineCallbacks, returnValue

log = logging.getLogger('PIPlanningServer')

REG_OPTIONS = RegisterOptions(match=u"wildcard", invoke=u'roundrobin',
                              details_arg='details')


class BaseAutobahnWAMPClient(object):
    base_uri = ''

    def __init__(self, wamp_session):
        self.wamp_session = wamp_session
        self.unregisters = list()
        self.unsubs = list()
        self.register()

    @inlineCallbacks
    def reg_event(self, func, event, options=REG_OPTIONS):
        reg = yield self.wamp_session.register(
            func, f'{self.base_uri}{event}', options=options)
        self.unregisters.append(reg)
        returnValue(reg)

    def register(self):
        pass

    @inlineCallbacks
    def remove(self):
        """
        Gets called as soon the room is empty.
        :return:
        """
        yield self.unregister()

        # Unsubscribe topics
        for sub in self.unsubs[:]:
            try:
                sub.unsubscribe()
            except Exception as error:
                log.error("Err while unsubsc: %s, %s" % (error, type(sub)))
            self.unsubs.remove(sub)

    @inlineCallbacks
    def unregister(self):
        """ Un-register all functions
        """
        for reg in self.unregisters:
            try:
                yield reg.unregister()
            except Exception as error:
                log.warning("Err while unregister from %s: %s, %s"
                            % (self, error, type(reg)))
