"""
Client module to interact with pitracer.
This will help you to send the log-entries to the redis queue.
"""
import os
import json
import time
from pi_server.services.rqueue import RedisQueue
from twisted.internet.defer import inlineCallbacks
import inspect

# Allow a easy circuit breaker (toggle) in production (without re-build)
DEACTIVATE_PI_TRACER = os.getenv("DEACTIVATE_PI_TRACER", False)
if DEACTIVATE_PI_TRACER in ("True", "1", "true"):
    DEACTIVATE_PI_TRACER = True
else:
    DEACTIVATE_PI_TRACER = False


class Tracer:
    def __init__(self, host, port, service):
        """
        :param host: redis hostname
        :param port: redis port
        :param service: the name of the service which uses this module
        """
        self.service = service
        self.logger = None
        self.queue = None
        if host and port:
            self.queue = RedisQueue(host, port)

    def init(self, host, port, service, logger=None):
        """
        :param host: redis hostname
        :param port: redis port
        :param service: the name of the service which uses this module
        :param logger: logger object (optional)
        :return:
        """
        if not self.queue:
            self.queue = RedisQueue(host, port)
        self.service = service
        self.logger = logger

    @inlineCallbacks
    def log(
        self,
        company,
        triggered_by,
        action,
        data={},
        log=True,
        wamp_details=None,
        **kwargs,
    ):
        """Pushes a new log entry to the queue. You can pass as many additional
        keyword-arguments as you like. These arguments will then be passed
        to the queue as well. All data needs to be json serializable.

        If self.log is set to a logger, it will also log that event.

        :param company: tenant name (required)
        :param triggered_by: by what service or whim got this triggered?
                             (required)
        :param action: what happens? (e.g. sticky_delete) (required)
        :param data: additional data (e.g. {'foo': 'bar'})
        :param log: default to True. If set to False, it will not log to local
                    logger (only to pitracer)
        :param wamp_details: You can pass the details argument which gets
                             you receive at each wamp-call. This will be used
                             to set 'authid'.
        """
        if DEACTIVATE_PI_TRACER:
            return
        # Find the caller (because of the defered, find first one which is not a
        # defered. The first element of stack() is this function here -> skip that.
        for frame in inspect.stack()[1:]:
            if frame.filename.endswith("defer.py"):
                continue
            lineno = frame.lineno
            filename = frame.filename.split("/")[-1]
            break
        else:
            lineno = 0
            filename = ""

        _log = {
            "timestamp": time.time(),
            "service": self.service,
            "company": company,
            "triggered_by": triggered_by,
            "action": action,
            "data": data,
        }
        _log.update(kwargs)
        authid = ""
        if wamp_details:
            _log["authid"] = authid = wamp_details.caller_authid
        # Write log messages if log is set
        if self.logger and log:
            self.logger.debug(
                f"{filename}:{lineno} [{company}] {action} "
                f'by "{triggered_by}"/{authid}: '
                f"{kwargs}; data={data}"
            )
        # Push to queue
        yield self.queue.put("logs", json.dumps(_log))


tracer = Tracer(None, None, None)
