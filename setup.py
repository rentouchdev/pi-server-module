"""
PI planning server module
-------------

Gathered common functionality and data to use in pi micro-services
"""
import codecs
import os.path
from setuptools import setup, find_packages


def read(rel_path):
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, rel_path), 'r') as fp:
        return fp.read()


def get_version(rel_path):
    for line in read(rel_path).splitlines():
        if line.startswith('__version__'):
            delim = '"' if '"' in line else "'"
            return line.split(delim)[1]
    else:
        raise RuntimeError("Unable to find version string.")


setup(
    name='pi_server',
    version=get_version('pi_server/__init__.py'),
    url='https://bitbucket.org/rentouchdev/pi-server-module',
    author='Rentouch',
    author_email='dominique.burnand@rentouch.ch',
    description='Common functionality and data to use in pi micro-services',
    packages=find_packages(),
    long_description=read('README.md'),
    python_requires='>=3.6.5',
    install_requires=[
        'Twisted>=15.2.1',
        'autobahn>=0.16',
        'txmongo>=18.2.0',
        'txredisapi>=1.4.7'
    ],
    classifiers=[
        "Topic :: Utilities",
    ]
)
